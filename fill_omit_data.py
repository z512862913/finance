import datetime


file_name_list = ['IF1703.csv', 'IF1612.csv']


def get_save_file_name(file_name):
    name_tmp = file_name.split('.')
    return name_tmp[0] + '_filled.' + name_tmp[1]


def serialize_line(line):
    if line:
        line = line.strip().strip('\ufeff').split(',')
        data = {
            'time': datetime.datetime.strptime(line[0], '%Y-%m-%d %H:%M'),
            'open': line[1],
            'max': line[2],
            'min': line[3],
            'close': line[4],
            'settlement': line[5],
            'turnover': line[6],
            'volume': line[7],
            'interest': line[8],
        }
    else:
        data = {
            'time': None,
            'open': None,
            'max': None,
            'min': None,
            'close': None,
            'settlement': None,
            'turnover': None,
            'volume': None,
            'interest': None,
        }
    return data


def to_string_data(data, time=None):
    if time:
        # 这里是补全的数据
        data_list = [time.strftime('%Y-%m-%d %H:%M'),
                     data['close'], data['close'], data['close'], data['close'], data['settlement'],
                     '0', '0', '0', ]
    else:
        data_list = [data['time'].strftime('%Y-%m-%d %H:%M'),
                     data['open'], data['max'], data['min'], data['close'], data['settlement'],
                     data['turnover'], data['volume'], data['interest'], ]
    return ','.join(data_list) + '\n'


class Filler(object):
    date_list = []
    input_file = None
    output_file = None

    def get_date_list(self):
        for line in self.input_file.readlines():
            data = serialize_line(line)
            current_date = data['time'].date()
            if current_date not in self.date_list:
                self.date_list.append(current_date)

    def __init__(self, input_file, output_file):
        self.input_file = input_file
        self.output_file = output_file
        self.get_date_list()
        input_file.seek(0)

    def fill_omit_date(self):
        morning_start_time = datetime.time(hour=9, minute=30)
        morning_end_time = datetime.time(hour=11, minute=30)
        afternoon_start_time = datetime.time(hour=13, minute=00)
        afternoon_end_time = datetime.time(hour=15, minute=00)
        pre_line_data = None
        line = self.input_file.readline()
        line_data = serialize_line(line)
        for date in self.date_list:
            today_morning_start_time = datetime.datetime.combine(date, morning_start_time)
            today_morning_end_time = datetime.datetime.combine(date, morning_end_time)
            today_afternoon_start_time = datetime.datetime.combine(date, afternoon_start_time)
            today_afternoon_end_time = datetime.datetime.combine(date, afternoon_end_time)
            for seconds in range(0, (today_morning_end_time-today_morning_start_time).seconds, 60):
                time_delta = datetime.timedelta(seconds=seconds)
                current_time = today_morning_start_time + time_delta
                while line_data['time'] < current_time:
                    line = self.input_file.readline()
                    line_data = serialize_line(line)
                if line_data['time'] > current_time:
                    self.output_file.write(to_string_data(pre_line_data, current_time))
                    continue
                self.output_file.write(to_string_data(line_data))
                pre_line_data = line_data
                line = self.input_file.readline()
                line_data = serialize_line(line)
            for seconds in range(0, (today_afternoon_end_time - today_afternoon_start_time).seconds, 60):
                time_delta = datetime.timedelta(seconds=seconds)
                current_time = today_afternoon_start_time + time_delta
                while line_data['time'] < current_time:
                    line = self.input_file.readline()
                    line_data = serialize_line(line)
                if line_data['time'] != current_time:
                    self.output_file.write(to_string_data(pre_line_data, current_time))
                    continue
                self.output_file.write(to_string_data(line_data))
                pre_line_data = line_data
                line = self.input_file.readline()
                line_data = serialize_line(line)


if __name__ == '__main__':
    for file_name in file_name_list:
        with open(file_name, encoding='utf-8') as input_file:
            with open(get_save_file_name(file_name), 'w') as output_file:
                filler = Filler(input_file=input_file, output_file=output_file)
                filler.fill_omit_date()

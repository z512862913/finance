import datetime
import copy
import json
from decimal import Decimal


class Simulator(object):
    p1_file_dir = 'IF1612_filled.csv'
    p2_file_dir = 'IF1703_filled.csv'
    build_data = range(0, 16560+1)
    test_data = range(16561, 21840+1)
    p1_data = []
    p2_data = []
    p1_close_data = []
    p2_close_data = []
    s_percentage_list = []
    s_list = []
    max_transaction_per_day = 5

    def __init__(self):
        self.get_close_data()

    @staticmethod
    def serialize_line(line):
        if line:
            line = line.strip().strip('\ufeff').split(',')
            data = {
                'time': datetime.datetime.strptime(line[0], '%Y-%m-%d %H:%M'),
                'open': line[1],
                'max': line[2],
                'min': line[3],
                'close': line[4],
                'settlement': line[5],
                'turnover': line[6],
                'volume': line[7],
                'interest': line[8],
            }
        else:
            data = {
                'time': None,
                'open': None,
                'max': None,
                'min': None,
                'close': None,
                'settlement': None,
                'turnover': None,
                'volume': None,
                'interest': None,
            }
        return data

    def get_close_data(self):
        with open(self.p1_file_dir) as p1_file:
            for line in p1_file.readlines():
                data = self.serialize_line(line)
                self.p1_close_data.append(float(data['close']))
                self.p1_data.append(data)
        with open(self.p2_file_dir) as p2_file:
            for line in p2_file.readlines():
                data = self.serialize_line(line)
                self.p2_close_data.append(float(data['close']))
                self.p2_data.append(data)

    def get_s_list(self):
        percent_spread_list = []
        for i in self.build_data:
            p1 = self.p1_close_data[i]
            p2 = self.p2_close_data[i]
            percent_spread = (p1 - p2) / (p1 + p2)
            percent_spread_list.append(percent_spread)
        percent_spread_list.sort()
        percent_spread_list_len = len(percent_spread_list)
        self.s_list = []
        for s_percentage in self.s_percentage_list:
            self.s_list.append(percent_spread_list[round(percent_spread_list_len * s_percentage)])
        # print(self.s_list)

    def split_build_and_test_data(self, test_start_date, test_end_date):
        start_index = 0
        end_index = 0
        for i in range(0, self.p1_data.__len__()):
            if not start_index and self.p1_data[i]['time'].date() == test_start_date:
                start_index = i
            if not end_index and self.p1_data[i]['time'].date() > test_end_date:
                end_index = i
                break
        self.test_data = range(0, start_index)
        self.test_data = range(start_index, end_index)

    @staticmethod
    def get_payoff_and_cost(transaction):
        transaction['payoff'] = (transaction['p1'][0] + transaction['p1'][1] + transaction['p2'][0] + transaction['p2'][
            1]) * 300 - \
                                (abs(transaction['p1'][0]) + abs(transaction['p1'][1]) +
                                 abs(transaction['p2'][0]) + abs(transaction['p2'][1])) * 300 * 0.000025
        transaction['cost'] = (max(abs(transaction['p1'][0]), abs(transaction['p1'][1])) +
                               max(abs(transaction['p2'][0]), abs(transaction['p2'][1]))) * 300 * 0.3
        return transaction

    def simulate(self):
        self.get_s_list()
        transaction_list = []
        empty_transaction = {
            'payoff': None,
            'time': [],
            'p1': [],
            'p2': [],
        }
        has_buy_p1_sell_p2 = False
        has_buy_p2_sell_p1 = False
        new_transaction = copy.deepcopy(empty_transaction)
        max_cost = 0.0
        total_payoff = 0.0
        date_trace = None
        current_transaction_count = 0
        # 下面正值为卖出，负值为买入
        for i in self.test_data:
            current_date = self.p1_data[i]['time'].date()
            if date_trace != current_date:
                date_trace = current_date
                current_transaction_count = 0
            p1 = self.p1_close_data[i]
            p2 = self.p2_close_data[i]
            percent_spread = (p1 - p2) / (p1 + p2)
            if has_buy_p1_sell_p2:
                if percent_spread < self.s_list[0] or percent_spread > self.s_list[2]:
                    new_transaction['time'].append(self.p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
                    new_transaction['p1'].append(p1)
                    new_transaction['p2'].append(-p2)
                    has_buy_p1_sell_p2 = False
                    self.get_payoff_and_cost(new_transaction)
                    transaction_list.append(new_transaction)
                    max_cost = max(max_cost, new_transaction['cost'])
                    total_payoff += new_transaction['payoff']
                    current_transaction_count += 1
                    new_transaction = copy.deepcopy(empty_transaction)
            elif has_buy_p2_sell_p1:
                if percent_spread < self.s_list[3] or percent_spread > self.s_list[5]:
                    new_transaction['time'].append(self.p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
                    new_transaction['p1'].append(-p1)
                    new_transaction['p2'].append(p2)
                    has_buy_p2_sell_p1 = False
                    self.get_payoff_and_cost(new_transaction)
                    transaction_list.append(new_transaction)
                    max_cost = max(max_cost, new_transaction['cost'])
                    total_payoff += new_transaction['payoff']
                    current_transaction_count += 1
                    new_transaction = copy.deepcopy(empty_transaction)
            elif current_transaction_count < self.max_transaction_per_day and percent_spread < self.s_list[1]:
                new_transaction['time'].append(self.p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
                new_transaction['p1'].append(-p1)
                new_transaction['p2'].append(p2)
                has_buy_p1_sell_p2 = True
            elif current_transaction_count < self.max_transaction_per_day and percent_spread > self.s_list[4]:
                new_transaction['time'].append(self.p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
                new_transaction['p1'].append(p1)
                new_transaction['p2'].append(-p2)
                has_buy_p2_sell_p1 = True
        return total_payoff / max_cost, transaction_list


def find_max_yield(test_start_date, test_end_date):
    max_yield = 0
    max_yield_combo = None
    simulator = Simulator()
    simulator.split_build_and_test_data(test_start_date, test_end_date)
    for a in range(1, 50):
        for b in range(a, 50):
            for c in range(b, 50):
                simulator.s_percentage_list = [Decimal(a) / 100, Decimal(b) / 100, Decimal(c) / 100,
                                               1 - Decimal(c) / 100, 1 - Decimal(b) / 100, 1 - Decimal(a) / 100]
                current_yield, transaction_data = simulator.simulate()
                if current_yield > max_yield:
                    max_yield = current_yield
                    max_yield_combo = simulator.s_percentage_list
    print(len(transaction_data))
    print(max_yield)
    print(max_yield_combo)
    json.dump(transaction_data, open('transaction.json', 'w'))


def test_current_combo(s0, s1, s2, test_start_date, test_end_date):
    combo = [s0, s1, s2, 1-s2, 1-s1, 1-s0]
    simulator = Simulator()
    simulator.s_percentage_list = combo
    simulator.split_build_and_test_data(test_start_date, test_end_date)
    current_yield, transaction_data = simulator.simulate()
    json.dump(transaction_data, open('transaction.json', 'w'))
    print(len(transaction_data))
    print(current_yield)


if __name__ == '__main__':
    start_date = datetime.date(year=2016, month=9, day=1)
    end_date = datetime.date(year=2016, month=9, day=30)
    # test_current_combo(0.02, 0.32, 0.43, start_date, end_date)
    find_max_yield(start_date, end_date)

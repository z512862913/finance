from fill_omit_data import serialize_line
import json
import copy

p1_file_dir = 'IF1612_filled.csv'
p2_file_dir = 'IF1703_filled.csv'

s_percentage_list = [0.02, 0.38, 0.38, 0.62, 0.62, 0.98]
# s_percentage_list = [0.02, 0.125, 0.875, 0.98]

build_data = range(0, 16560+1)
test_data = range(16561, 21840+1)

# get_data
p1_data = []
with open(p1_file_dir) as p1_file:
    for line in p1_file.readlines():
        p1_data.append(serialize_line(line))
p2_data = []
with open(p2_file_dir) as p2_file:
    for line in p2_file.readlines():
        p2_data.append(serialize_line(line))
p1_close_data = list(float(j['close']) for j in p1_data)
p2_close_data = list(float(j['close']) for j in p2_data)


# get s_list
percent_spread_list = []
for i in build_data:
    p1 = p1_close_data[i]
    p2 = p2_close_data[i]
    percent_spread = (p1 - p2) / (p1 + p2)
    percent_spread_list.append(percent_spread)
percent_spread_list.sort()
percent_spread_list_len = len(percent_spread_list)
s_list = []
for s_percentage in s_percentage_list:
    s_list.append(percent_spread_list[round(percent_spread_list_len*s_percentage)])
print(s_list)

# # get_multiple
# multiple_p1 = 1
# multiple_p2 = 1
# p1_std = numpy.std(p1_close_data[0: build_data.stop], ddof=1)
# p2_std = numpy.std(p2_close_data[0: build_data.stop], ddof=1)
# multiple_p2 = round(p1_std / p2_std)
# print(p1_std)
# print(p2_std)
# print(multiple_p2)
# always 1


def get_payoff_and_cost(transaction):
    transaction['payoff'] = (transaction['p1'][0] + transaction['p1'][1] + transaction['p2'][0] + transaction['p2'][1]) * 300 - \
      (abs(transaction['p1'][0]) + abs(transaction['p1'][1]) +
       abs(transaction['p2'][0]) + abs(transaction['p2'][1])) * 300 * 0.000025
    transaction['cost'] = (max(abs(transaction['p1'][0]), abs(transaction['p1'][1])) +
                           max(abs(transaction['p2'][0]), abs(transaction['p2'][1]))) * 300 * 0.3
    return transaction

# simulation
transaction_list = []
empty_transaction = {
        'payoff': None,
        'time': [],
        'p1': [],
        'p2': [],
    }
has_buy_p1_sell_p2 = False
has_buy_p2_sell_p1 = False
new_transaction = copy.deepcopy(empty_transaction)
max_cost = 0
total_payoff = 0
# 下面正值为卖出，负值为买入
for i in test_data:
    p1 = p1_close_data[i]
    p2 = p2_close_data[i]
    percent_spread = (p1 - p2) / (p1 + p2)
    if has_buy_p1_sell_p2:
        if percent_spread < s_list[0] or percent_spread > s_list[2]:
            new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
            new_transaction['p1'].append(p1)
            new_transaction['p2'].append(-p2)
            has_buy_p1_sell_p2 = False
            get_payoff_and_cost(new_transaction)
            transaction_list.append(new_transaction)
            max_cost = max(max_cost, new_transaction['cost'])
            total_payoff += new_transaction['payoff']
            new_transaction = copy.deepcopy(empty_transaction)
    elif has_buy_p2_sell_p1:
        if percent_spread < s_list[3] or percent_spread > s_list[5]:
            new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
            new_transaction['p1'].append(-p1)
            new_transaction['p2'].append(p2)
            has_buy_p2_sell_p1 = False
            get_payoff_and_cost(new_transaction)
            transaction_list.append(new_transaction)
            max_cost = max(max_cost, new_transaction['cost'])
            total_payoff += new_transaction['payoff']
            new_transaction = copy.deepcopy(empty_transaction)
    elif percent_spread < s_list[1]:
        new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
        new_transaction['p1'].append(-p1)
        new_transaction['p2'].append(p2)
        has_buy_p1_sell_p2 = True
    elif percent_spread > s_list[4]:
        new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
        new_transaction['p1'].append(p1)
        new_transaction['p2'].append(-p2)
        has_buy_p2_sell_p1 = True
# for i in test_data:
#     p1 = p1_close_data[i]
#     p2 = p2_close_data[i]
#     percent_spread = (p1 - p2) / (p1 + p2)
#     if has_buy_p1_sell_p2:
#         if percent_spread < s_list[0] or percent_spread > s_list[1]:
#             new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
#             new_transaction['p1'].append(p1)
#             new_transaction['p2'].append(-p2)
#             has_buy_p1_sell_p2 = False
#             get_payoff_and_cost(new_transaction)
#             transaction_list.append(new_transaction)
#             max_cost = max(max_cost, new_transaction['cost'])
#             total_payoff += new_transaction['payoff']
#             new_transaction = copy.deepcopy(empty_transaction)
#     elif has_buy_p2_sell_p1:
#         if percent_spread < s_list[2] or percent_spread > s_list[3]:
#             new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
#             new_transaction['p1'].append(-p1)
#             new_transaction['p2'].append(p2)
#             has_buy_p2_sell_p1 = False
#             get_payoff_and_cost(new_transaction)
#             transaction_list.append(new_transaction)
#             max_cost = max(max_cost, new_transaction['cost'])
#             total_payoff += new_transaction['payoff']
#             new_transaction = copy.deepcopy(empty_transaction)
#     elif percent_spread < s_list[1]:
#         new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
#         new_transaction['p1'].append(-p1)
#         new_transaction['p2'].append(p2)
#         has_buy_p1_sell_p2 = True
#     elif percent_spread > s_list[2]:
#         new_transaction['time'].append(p1_data[i]['time'].strftime('%Y-%m-%d %H:%M'))
#         new_transaction['p1'].append(p1)
#         new_transaction['p2'].append(-p2)
#         has_buy_p2_sell_p1 = True


# for i in test_data:
#     p1 = p1_close_data[i]
#     p2 = p2_close_data[i]
#     percent_spread = (p1 - p2) / (p1 + p2)
#     if has_buy_p1_sell_p2:
#         if percent_spread < s_list[0] or percent_spread > s_list[2]:
#             new_transaction['p1'].append(-p1)
#             new_transaction['p2'].append(p2)
#             has_buy_p1_sell_p2 = False
#             transaction_list.append(new_transaction)
#             new_transaction = copy.deepcopy(empty_transaction)
#     elif has_buy_p2_sell_p1:
#         if percent_spread < s_list[3] or percent_spread > s_list[5]:
#             new_transaction['p1'].append(p1)
#             new_transaction['p2'].append(-p2)
#             has_buy_p2_sell_p1 = False
#             transaction_list.append(new_transaction)
#             new_transaction = copy.deepcopy(empty_transaction)
#     elif percent_spread < s_list[1]:
#         new_transaction['p1'].append(p1)
#         new_transaction['p2'].append(-p2)
#         has_buy_p1_sell_p2 = True
#     elif percent_spread > s_list[4]:
#         new_transaction['p1'].append(-p1)
#         new_transaction['p2'].append(p2)
#         has_buy_p2_sell_p1 = True
print(len(transaction_list))
json.dump(transaction_list, open('transaction.json', 'w'))
print(max_cost)
print(total_payoff)
print(total_payoff / max_cost)




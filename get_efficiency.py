import json

file_name = 'transaction_02_32_43_Sep_16.json'

transactions = json.load(open(file_name))

success_count = 0
for transaction in transactions:
    if transaction['payoff'] > 0:
        success_count += 1

print(success_count / transactions.__len__())
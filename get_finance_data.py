import requests
import json
import datetime
import csv

url = 'http://chartapi.finance.yahoo.com/instrument/1.0/%5EGSPC/chartdata;type=quote;range=15d/json'
s = requests.get(url).content.decode()
s = s[s.find('{'):]
s = s.strip(')')
data = json.loads(s)['series']

with open('S&P500_15day_per_5minutes.csv', 'w') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['time', 'open', 'high', 'low', 'close', 'volume'])
    for line in data:
        time = str(datetime.datetime.fromtimestamp(line['Timestamp']))
        spamwriter.writerow([time, line['open'], line['high'], line['low'], line['close'], line['volume']])

pass